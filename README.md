# Specification - Resep Masakan

## Prerequisite
- Java jdk 17.
- Maven 3.6 keatas.
- PostgreSQL 12 Keatas.
- IDE, IntelliJ, VsCode atau Eclipse.
- Pgadmin atau dbeaver.
- Postman / Thunder Client (Vs Code Extenstions).
- Database Resep Masakan.

Siapkan Environment yang dibutuhkan diatas, Jika belum install bisa cek link berikut ini : https://gitlab.com/ideki11/springboot-set-up-environment

# Guide Step by step membuat Project Springboot

Bagi yang belum membuat Struktur Project yang dibutuhkan, silahkan cek link berikut ini : https://gitlab.com/ideki11/structur-project-springboot-resep-masakan

# Guide Step by step membuat Standard CRUD API

Bagi yang belum membuat Standard CRUD API, silahkan cek link Berikut ini: https://gitlab.com/ideki11/standard-crud-springboot-resep-masakan

# Guide Step by step JWT Auth Springboot

Bagi yang belum membuat JWT Auth Springboot, silahkan cek link Berikut ini: https://gitlab.com/ideki11/jwt-auth-springboot-resep-masakan

## Step by step Specification pada Springboot

> # DTO

1. Tambahkan sebuah DTO baru dengan nama FilterRecipeRequest.java pada package "request", seperti contoh berikut ini:
    ```java
        import lombok.AllArgsConstructor;
        import lombok.Builder;
        import lombok.Data;
        import lombok.NoArgsConstructor;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        public class FilterRecipeRequest {
            private String recipeName;
            private Integer levelId;
            private Integer categoryId;
            private String timeCook;
            private String sortBy;
        }
    ```

> # Repository
1. Pada RecipeRepository.java kita tambahkan interface Specification (JpaSpecificationExecutor), seperti contoh berikut ini:
    ```java
        import org.springframework.data.domain.Page;
        import org.springframework.data.domain.Pageable;
        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
        import org.springframework.stereotype.Repository;

        import com.example.resep.models.Recipe;

        @Repository
        public interface RecipeRepository extends JpaRepository<Recipe, Integer>, JpaSpecificationExecutor<Recipe>{
            Page<Recipe> findAll(Pageable pageable);
        }
    ```
    - Kodingan di atas adalah sebuah interface yang merupakan bagian dari aplikasi Spring Data JPA di dalam framework Spring Boot. Interface ini bertugas untuk melakukan operasi CRUD (Create, Read, Update, Delete) dan pencarian dengan kriteria (criteria queries) pada entitas Recipe.

    - RecipeRepository extends dua interface dari Spring Data JPA:
        1. JpaRepository<Recipe, Integer>:
            - JpaRepository adalah interface generik dari Spring Data JPA yang menyediakan metode dasar untuk operasi CRUD dan pagination pada entitas.
            - Recipe adalah tipe entitas yang dikelola oleh repository ini.
            - Integer adalah tipe dari primary key (identifier) dari entitas Recipe.
        2. JpaSpecificationExecutor<Recipe>:
            - JpaSpecificationExecutor adalah interface yang menyediakan dukungan untuk pencarian dengan kriteria (criteria queries).
            - Mengizinkan eksekusi query dengan spesifikasi yang dinamis pada entitas Recipe.
    
    - Fungsi Keseluruhan
        * RecipeRepository menyediakan metode dasar untuk operasi CRUD, dukungan untuk pencarian dengan kriteria, dan kemampuan untuk melakukan pagination pada entitas Recipe.
        * Dengan extends JpaRepository dan JpaSpecificationExecutor, repository ini secara otomatis mewarisi banyak metode bawaan dari Spring Data JPA, seperti save(), delete(), findById(), dan juga metode untuk pencarian kriteria (Specification) yang lebih kompleks.
    
> # Specifications

1. Buatlah package baru dengan nama "specifications" di package "services".
2. Buatlah sebuah class baru dengan nama RecipeSpecification.java, seperti contoh berikut ini:
    ```java
        import org.springframework.data.jpa.domain.Specification;

        import com.example.resep.dtos.requests.FilterRecipeRequest;
        import com.example.resep.models.Recipe;

        import jakarta.persistence.criteria.Predicate;

        public class RecipeSpecification {
            
            public static Specification<Recipe> recipesFilterAll(FilterRecipeRequest recipeRequestDTO) {
                return (root, query, criteriaBuilder) -> {
                    Predicate predicate = criteriaBuilder.conjunction();
                    if (recipeRequestDTO.getRecipeName() != null) {
                        predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(
                                criteriaBuilder.lower(root.get("recipeName")),
                                "%" + recipeRequestDTO.getRecipeName().toLowerCase() + "%"));
                    }
                    if (recipeRequestDTO.getLevelId() != null) {
                        predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(
                                root.get("level").get("levelId"), recipeRequestDTO.getLevelId()));
                    }
                    if (recipeRequestDTO.getCategoryId() != null) {
                        predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(
                                root.get("category").get("categoryId"), recipeRequestDTO.getCategoryId()));
                    }
                    if (recipeRequestDTO.getTimeCook() != null) {
                        String timeCook = recipeRequestDTO.getTimeCook();
                        if (timeCook.equals("0-30")) {
                            predicate = criteriaBuilder.and(predicate, criteriaBuilder.lessThanOrEqualTo(
                                    root.get("timeCook"), 30));
                        } else if (timeCook.equals("30-60")) {
                            predicate = criteriaBuilder.and(predicate, criteriaBuilder.between(
                                    root.get("timeCook"), 31, 60));
                        } else if (timeCook.equals("60")) {
                            predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThan(
                                    root.get("timeCook"), 60));
                        }
                    }
                    return predicate;
                };
            }
        }
    ```
    - Kelas RecipeSpecification yang berisi metode recipesFilterAll untuk membuat spesifikasi pencarian dinamis menggunakan Criteria API dari JPA. Spesifikasi ini memungkinkan pencarian entitas Recipe berdasarkan beberapa kriteria yang diberikan melalui objek FilterRecipeRequest (Request DTO yang sudah dipersiapkan diawal).

    - Metode recipesFilterAll
        * recipesFilterAll adalah metode statis yang menerima parameter FilterRecipeRequest bernama recipeRequestDTO.
        * Metode ini mengembalikan objek Specification<Recipe>, yang merupakan interface untuk mendefinisikan kriteria pencarian pada entitas Recipe.
    - Implementasi Spesifikasi
        ```java
            return (root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.conjunction();
        ```
        * Metode ini menggunakan lambda expression untuk mengimplementasikan metode toPredicate dari interface Specification.
        * `root` mewakili entitas root (Recipe) dalam query.
        * `query` mewakili objek query itu sendiri.
        * `criteriaBuilder` digunakan untuk membangun kriteria query.
        * `Predicate predicate = criteriaBuilder.conjunction()`; membuat predicate awal yang selalu benar (true), digunakan sebagai dasar untuk membangun predicate lainnya.
    
    - Menambahkan Kriteria Pencarian
        1. recipeName
            ```java
                if (recipeRequestDTO.getRecipeName() != null) {
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(
                            criteriaBuilder.lower(root.get("recipeName")),
                            "%" + recipeRequestDTO.getRecipeName().toLowerCase() + "%"));
                }
            ```
            * Jika `recipeRequestDTO` memiliki nilai `recipeName` yang tidak null, tambahkan predicate untuk mencari nama resep yang mengandung substring tertentu (LIKE).
            * Menggunakan `criteriaBuilder.lower` untuk mengubah nama resep menjadi huruf kecil untuk pencarian case-insensitive.
        2. levelId
            ```java
                if (recipeRequestDTO.getLevelId() != null) {
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(
                            root.get("level").get("levelId"), recipeRequestDTO.getLevelId()));
                }
            ```
            * Jika `recipeRequestDTO` memiliki `levelId` yang tidak null, tambahkan predicate untuk mencocokkan levelId pada entitas Recipe.
        
        3. categoryId
            ```java
                if (recipeRequestDTO.getCategoryId() != null) {
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(
                            root.get("category").get("categoryId"), recipeRequestDTO.getCategoryId()));
                }
            ```
            * Jika `recipeRequestDTO` memiliki `categoryId` yang tidak null, tambahkan predicate untuk mencocokkan categoryId pada entitas Recipe.

        4. timeCook
            ```java
                if (recipeRequestDTO.getTimeCook() != null) {
                    String timeCook = recipeRequestDTO.getTimeCook();
                    if (timeCook.equals("0-30")) {
                        predicate = criteriaBuilder.and(predicate, criteriaBuilder.lessThanOrEqualTo(
                                root.get("timeCook"), 30));
                    } else if (timeCook.equals("30-60")) {
                        predicate = criteriaBuilder.and(predicate, criteriaBuilder.between(
                                root.get("timeCook"), 31, 60));
                    } else if (timeCook.equals("60")) {
                        predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThan(
                                root.get("timeCook"), 60));
                    }
                }
            ```
            * Jika `recipeRequestDTO` memiliki nilai `timeCook` yang tidak null, tambahkan predicate berdasarkan nilai waktu memasak.
                - Jika timeCook bernilai "0-30", tambahkan predicate untuk resep dengan waktu memasak kurang dari atau sama dengan 30 menit.
                - Jika timeCook bernilai "30-60", tambahkan predicate untuk resep dengan waktu memasak antara 31 hingga 60 menit.
                - Jika timeCook bernilai "60", tambahkan predicate untuk resep dengan waktu memasak lebih dari 60 menit.
    
    - Kemudian di return dalam bentuk `prdicate`.
    - Fungsi Keseluruhan
        * Metode recipesFilterAll digunakan untuk membangun spesifikasi pencarian dinamis berdasarkan berbagai kriteria yang diterima dalam objek FilterRecipeRequest.
        * Dengan menggunakan Specification<Recipe>, metode ini memungkinkan penggunaan kriteria pencarian yang fleksibel dan dinamis dalam query JPA, memungkinkan pengguna untuk menyaring hasil berdasarkan nama resep, tingkat kesulitan, kategori, waktu memasak, dll.
    

3. Pada class RecipeService.java, kita bisa tambahkan atau kita modifikasi service Read All Recipes atau Get All Recipes.
4. Disini sebagai contoh, akan ditambahkan sebuah service baru yaitu get all recipe yang sudah menggunakan specification agar bisa menjadi contoh atau pembanding antara yang menggunakan Specification dan yang tidak (Standar Read/Get data). Seperti contoh berikut ini:
    ```java
        //Read Recipes with filter or Specification
        public ResponseEntity<Object> getAllRecipe(FilterRecipeRequest recipeRequestDTO, Pageable page) {
            HttpStatus status = HttpStatus.OK;
            
            // Function digunakan untuk membuat filter pada query recipes berdasarkan nilai dari recipeRequestDTO
            Specification<Recipe> spec = RecipeSpecification.recipesFilterAll(recipeRequestDTO);
            // Function digunakan untuk mencari semua data resep, ditambah dengan filter yang sudah dibuat
            Page<Recipe> filteredRecipeList = recipeRepo.findAll(spec, page);

            // Function yang digunakan untuk merubah tipe data dari "Page<Recipes>" ke List<RecipesDTO>
            List<RecipeDto> recipesDTOList = filteredRecipeList.stream()
                                        .map(recipe -> mapper.map(recipe, RecipeDto.class))
                                        .collect(Collectors.toList());

            ResponseDto response = ResponseDto.builder()
                                        .data(recipesDTOList)
                                        .message("Read All Recipe Success.")
                                        .status(status.toString())
                                        .statusCode(status.value())
                                        .total(recipesDTOList.size())
                                        .build();

            return ResponseEntity.status(status).body(response);
        }
    ```

5. Pada class RecipeController.java kita tambahkan endpoin API baru untuk API Get Recipe yang sudah menggunakan specification. Seperti contoh berikut ini:
    ```java
        //read all recipe with specification
        @GetMapping("/get-all-recipes")
        public ResponseEntity<Object> getAllRecipe(FilterRecipeRequest recipeRequestDTO,
            @PageableDefault(page = 0, size = 8) Pageable page,
            @RequestParam(defaultValue = "recipeName") String sortBy,
            @RequestParam(defaultValue = "ASC") Direction direction) {

            Pageable sortedPage = PageRequest.of(page.getPageNumber(), page.getPageSize(), direction, sortBy);
            return recipeService.getAllRecipe(recipeRequestDTO, sortedPage);
        }
    ```

> # Test API di Postman

1. Untuk menguji API yang sudah dibuat, kita bisa menggunakan Postman atau aplikasi thunder client.
2. Untuk penggunaan specification pada API yang kita buat bisa melalui param, seperti contoh berikut ini:
    ![GetAllRecipeSpecification][def1]

    [def1]: img/GetAllRecipeSpecification.PNG

    - tinggal di atur filter yang akan digunakan untuk pencarian Recipe pada 'param', misal ingin mencari recipe berdasarkan recipeName dan category. Maka kita tinggal isi param untuk recipeName dan categoryId nya.

