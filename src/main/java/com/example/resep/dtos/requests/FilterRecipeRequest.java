package com.example.resep.dtos.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilterRecipeRequest {
    private String recipeName;
	private Integer levelId;
	private Integer categoryId;
	private String timeCook;
	private String sortBy;
}
