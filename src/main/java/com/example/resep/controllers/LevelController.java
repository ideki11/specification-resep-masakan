package com.example.resep.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.resep.services.LevelService;

@RestController
@RequestMapping("/api/level")
public class LevelController {
	@Autowired
	LevelService levelService;
	
	//API read all Level
	@GetMapping("/read-all-level")
	public ResponseEntity<Object> readAllLevel(){
		return levelService.readAllLevel();
	}
}
