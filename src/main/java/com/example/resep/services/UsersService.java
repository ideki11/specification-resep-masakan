package com.example.resep.services;

import java.util.Optional;
import java.util.Set;

import org.aspectj.bridge.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.resep.dtos.ResponseDto;
import com.example.resep.dtos.requests.LoginRequest;
import com.example.resep.dtos.requests.RegisterRequest;
import com.example.resep.dtos.responses.JwtResponse;
import com.example.resep.models.Users;
import com.example.resep.repositories.UsersRepository;
import com.example.resep.security.jwt.JwtUtils;
import com.example.resep.security.services.UserDetailsImplement;


import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import lombok.extern.slf4j.Slf4j;


@Service
@Transactional
@Slf4j
public class UsersService {
	@Autowired
    private UsersRepository userRepository;
    
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;
    
    @Autowired
    private Validator validator;

    @Autowired
    JwtUtils jwtUtils;

    final HttpStatus statusOK = HttpStatus.OK;
    
    public ResponseDto register(RegisterRequest registerRequest) {
    	 Set<ConstraintViolation<RegisterRequest>> constraintViolations = validator.validate(registerRequest);

         if(!constraintViolations.isEmpty()){
             ConstraintViolation<RegisterRequest> firstViolation = constraintViolations.iterator().next();
             String errorMessage = firstViolation.getMessage();
             return ResponseDto.builder()
     				.message(errorMessage)
     				.status("ERROR")
     				.statusCode(HttpStatus.BAD_REQUEST.value())
     				.build();
         }
    	
    	if(userRepository.existsByUsername(registerRequest.getUsername())){
            String errorMessage = "application.error.already-exist.user";
            return ResponseDto.builder()
            				.message(errorMessage)
            				.status("ERROR")
            				.statusCode(HttpStatus.BAD_REQUEST.value())
            				.build();
        }
    	
    	if (!registerRequest.getPassword().equals(registerRequest.getRetypePassword())) {
            String errorMessage = "application.error.password-not-match.user";
            return ResponseDto.builder()
    				.message(errorMessage)
    				.status("ERROR")
    				.statusCode(HttpStatus.BAD_REQUEST.value())
    				.build();
        }
    	
    	if(registerRequest.getPassword().length() < 6){
            String errorMessage = "application.error.password-validation.user";
            return ResponseDto.builder()
    				.message(errorMessage)
    				.status("ERROR")
    				.statusCode(HttpStatus.BAD_REQUEST.value())
    				.build();
        }
    	
    	//mapping data dari request ke entity
    	Users user = Users.builder()
    					.username(registerRequest.getUsername())
    					.fullname(registerRequest.getFullname())
    					.password(BCrypt.hashpw(registerRequest.getPassword(), BCrypt.gensalt()))
    					.role("User")
    					.build();
    	
    	//save data entity
    	userRepository.save(user);
    	
    	log.info("Received user: {}", user);
        
        String successMessage = "application.success.add.user";
		return ResponseDto.builder()
				.message(successMessage)
				.status("OK")
				.statusCode(HttpStatus.OK.value())
				.build();
    }
    
    public ResponseDto signIn(LoginRequest loginRequest){
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
            
            SecurityContextHolder.getContext().setAuthentication(authentication);
            
            String jwt = jwtUtils.generateJwtToken(authentication);
            
            UserDetailsImplement userDetails = (UserDetailsImplement) authentication.getPrincipal();    
            Optional<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .findFirst();
            
            log.info("Login Success!");
            
            return ResponseDto.builder()
                .data(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), roles.get()))
                .message("application.success.auth.user")
                .statusCode(statusOK.value())
                .status(statusOK.toString())
                .build();   
        } catch (AuthenticationException e){
            return ResponseDto.builder()
                .message(e.toString())
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .status(HttpStatus.UNAUTHORIZED.toString())
                .build();
        }catch (Exception e) {
            return ResponseDto.builder()
                .message("application.error.internal")
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .status(HttpStatus.INTERNAL_SERVER_ERROR.toString())
                .build();
        }
    }
}
