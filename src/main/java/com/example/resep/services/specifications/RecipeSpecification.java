package com.example.resep.services.specifications;

import org.springframework.data.jpa.domain.Specification;

import com.example.resep.dtos.requests.FilterRecipeRequest;
import com.example.resep.models.Recipe;

import jakarta.persistence.criteria.Predicate;

public class RecipeSpecification {
    
    public static Specification<Recipe> recipesFilterAll(FilterRecipeRequest recipeRequestDTO) {
        return (root, query, criteriaBuilder) -> {
            Predicate predicate = criteriaBuilder.conjunction();
            if (recipeRequestDTO.getRecipeName() != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("recipeName")),
                        "%" + recipeRequestDTO.getRecipeName().toLowerCase() + "%"));
            }
            if (recipeRequestDTO.getLevelId() != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(
                        root.get("level").get("levelId"), recipeRequestDTO.getLevelId()));
            }
            if (recipeRequestDTO.getCategoryId() != null) {
                predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(
                        root.get("category").get("categoryId"), recipeRequestDTO.getCategoryId()));
            }
            if (recipeRequestDTO.getTimeCook() != null) {
                String timeCook = recipeRequestDTO.getTimeCook();
                if (timeCook.equals("0-30")) {
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.lessThanOrEqualTo(
                            root.get("timeCook"), 30));
                } else if (timeCook.equals("30-60")) {
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.between(
                            root.get("timeCook"), 31, 60));
                } else if (timeCook.equals("60")) {
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThan(
                            root.get("timeCook"), 60));
                }
            }
            return predicate;
        };
    }
}
