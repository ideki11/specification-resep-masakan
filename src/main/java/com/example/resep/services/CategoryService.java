package com.example.resep.services;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.resep.dtos.CategoryDto;
import com.example.resep.dtos.ResponseDto;
import com.example.resep.exceptions.NotFoundException;
import com.example.resep.models.Category;
import com.example.resep.repositories.CategoryRepository;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class CategoryService {
	@Autowired
	CategoryRepository categoryRepo;

	ModelMapper mapper = new ModelMapper();
	
	public ResponseEntity<Object> readAllCategory() {
		//get all data category
		List<Category> listAllCategory = categoryRepo.findAll();	
		HttpStatus status = HttpStatus.OK;
		
		if (listAllCategory.isEmpty()) {
			throw new NotFoundException("Category", "Data", "Data is Empty.");
		}else {
			List<CategoryDto> listAllCategoryDto = new ArrayList<CategoryDto>();
			listAllCategoryDto = listAllCategory.stream()
												.map(categoryEntity -> mapper.map(categoryEntity, CategoryDto.class))
												.toList();
			ResponseDto response = ResponseDto.builder()
													.message("Read all Category Succes.")
													.data(listAllCategoryDto)
													.status(status.toString())
													.statusCode(status.value())
													.total(listAllCategoryDto.size())
													.build();
			
			return ResponseEntity.status(status).body(response);
		}
		
	}
	
}
