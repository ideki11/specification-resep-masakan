package com.example.resep.services;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.resep.dtos.RecipeDto;
import com.example.resep.dtos.ResponseDto;
import com.example.resep.dtos.requests.FilterRecipeRequest;
import com.example.resep.exceptions.NotFoundException;
import com.example.resep.models.Category;
import com.example.resep.models.Level;
import com.example.resep.models.Recipe;
import com.example.resep.models.Users;
import com.example.resep.repositories.CategoryRepository;
import com.example.resep.repositories.LevelRepository;
import com.example.resep.repositories.RecipeRepository;
import com.example.resep.repositories.UsersRepository;
import com.example.resep.services.specifications.RecipeSpecification;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class RecipeService {
	@Autowired
	RecipeRepository recipeRepo;
	
	@Autowired
	LevelRepository levelRepo;
	
	@Autowired
	CategoryRepository categoryRepo;
	
	@Autowired
	UsersRepository usersRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	//Read All
	public ResponseEntity<Object> readAllRecipe(int size, int page){
		HttpStatus status = HttpStatus.OK;
		
		//get all data recepi
		List<Recipe> listAllRecepiEntity = recipeRepo.findAll(PageRequest.of(page, size)).toList();
		
		if (listAllRecepiEntity.isEmpty()) {
			throw new NotFoundException("Recepi", "Data", "Data is Empty");
		}else {
			//konversi all entity to DTO
			List<RecipeDto> listAllRecipeDto = listAllRecepiEntity.stream()
																.map(entity -> mapper.map(entity, RecipeDto.class))
																.toList();
			
			ResponseDto response = ResponseDto.builder()
													.data(listAllRecipeDto)
													.message("Read All Recipe Success.")
													.status(status.toString())
													.statusCode(status.value())
													.total(listAllRecipeDto.size())
													.build();
			
			return ResponseEntity.status(status).body(response);
		}
	}

	//Read Recipes with filter or Specification
	public ResponseEntity<Object> getAllRecipe(FilterRecipeRequest recipeRequestDTO, Pageable page) {
		HttpStatus status = HttpStatus.OK;
		
        // Function digunakan untuk membuat filter pada query recipes berdasarkan nilai dari recipeRequestDTO
        Specification<Recipe> spec = RecipeSpecification.recipesFilterAll(recipeRequestDTO);
        // Function digunakan untuk mencari semua data resep, ditambah dengan filter yang sudah dibuat
        Page<Recipe> filteredRecipeList = recipeRepo.findAll(spec, page);

        /*
         * Kondisi filtering berdasarkan comment contoh di "RecipeSpesification.recipesFilterAll"
         * query:
         *      SELECT * FROM recipe r
         *      WHERE r.recipe_name LIKE "%goreng%
         *      AND WHERE recipes.level_id = 2
         *      AND WHERE recipes.categories_id = 3
         *      AND WHERE recipes.time_cook > 60
         */

        // Function yang digunakan untuk merubah tipe data dari "Page<Recipes>" ke List<RecipesDTO>
        List<RecipeDto> recipesDTOList = filteredRecipeList.stream()
                                    .map(recipe -> mapper.map(recipe, RecipeDto.class))
                                    .collect(Collectors.toList());

		ResponseDto response = ResponseDto.builder()
									.data(recipesDTOList)
									.message("Read All Recipe Success.")
									.status(status.toString())
									.statusCode(status.value())
									.total(recipesDTOList.size())
									.build();

		return ResponseEntity.status(status).body(response);
    }
	
	//Read by recipe id
	public ResponseEntity<Object> readRecipeById(int recipeId){
		HttpStatus status = HttpStatus.OK;
		
		//get all data recepi
		Recipe recipeEntity = recipeRepo.findById(recipeId).orElseThrow(() -> new NotFoundException("Recipe", "recipe Id", recipeId));
		
		//konversi entity to dto
		RecipeDto recipeDto = mapper.map(recipeEntity, RecipeDto.class);

		ResponseDto response = ResponseDto.builder()
												.data(recipeDto)
												.message("Read Recipe By Id Success.")
												.status(status.toString())
												.statusCode(status.value())
												.total(1)
												.build();
			
		return ResponseEntity.status(status).body(response);
	}
	
	//create Recipe
	public ResponseEntity<Object> createRecipe(RecipeDto body){
		//validasi data
		Level levelEntity = levelRepo.findById(body.getLevel().getLevelId()).orElseThrow(() -> new NotFoundException("Level", "level Id", body.getLevel().getLevelId()));
		Category categoryEntity = categoryRepo.findById(body.getCategory().getCategoryId()).orElseThrow(() -> new NotFoundException("Category", "category Id", body.getCategory().getCategoryId()));
		Users userEntity = usersRepo.findById(body.getUsers().getUserId()).orElseThrow(()-> new NotFoundException("Users", "user id", body.getUsers().getUserId()));
		
		//create Recipe entity, konversi ke entity dengan modelmapper
		Recipe recipeEntity = mapper.map(body, Recipe.class);
							
		//save data recipe
		recipeRepo.save(recipeEntity);
		
		String responseMessage = "application.success.create.recipe";
        HttpStatus status = HttpStatus.CREATED;
		
		return ResponseEntity.status(status).body(responseMessage);
	}
	
	//update recipe
	public ResponseEntity<Object> editRecipe(int recipeId, RecipeDto body){
		//validasi data
		Recipe recipe = recipeRepo.findById(recipeId).orElseThrow(()-> new NotFoundException("Recipe", "recipe id", recipeId));
		Level levelEntity = levelRepo.findById(body.getLevel().getLevelId()).orElseThrow(() -> new NotFoundException("Level", "level Id", body.getLevel().getLevelId()));
		Category categoryEntity = categoryRepo.findById(body.getCategory().getCategoryId()).orElseThrow(() -> new NotFoundException("Category", "category Id", body.getCategory().getCategoryId()));
		Users userEntity = usersRepo.findById(body.getUsers().getUserId()).orElseThrow(()-> new NotFoundException("Users", "user id", body.getUsers().getUserId()));
		
		recipe.setCategory(categoryEntity);
		recipe.setLevel(levelEntity);
		recipe.setUsers(userEntity);
		recipe.setRecipeName(body.getRecipeName());
		recipe.setHowToCook(body.getHowToCook());
		recipe.setImageFilename(body.getImageFilename());
		recipe.setIngredient(body.getIngredient());
		recipe.setTimeCook(body.getTimeCook());
							
		//save data recipe
		recipeRepo.save(recipe);
		
		String responseMessage = "application.success.update.recipe";
        HttpStatus status = HttpStatus.OK;
		
		return ResponseEntity.status(status).body(responseMessage);
	}
	
	//detele recipe
	public ResponseEntity<Object> deleteRecipe(int recipeId){
		Recipe recipe = recipeRepo.findById(recipeId).orElseThrow(()-> new NotFoundException("Recipe", "recipe id", recipeId));
		
		//delete
		recipeRepo.delete(recipe);
		
		String responseMessage = "application.success.delete.recipe";
        HttpStatus status = HttpStatus.OK;
		
		return ResponseEntity.status(status).body(responseMessage);
	}
}
